describe('login app', function(){
  // var ptor = protractor.getInstance();

  it('should take you to the login page when accessing a restricted page and not logged in', function(){
    browser.get('#/about');
    expect(element(by.tagName('h1')).getText()).toBe('Web App Login');
  });

  it('should take you to the home page after login', function(){
    browser.get('#/');
    element(by.buttonText('Login')).click();
    expect(element(by.tagName('h1')).getText()).toBe('Home');
  });
});