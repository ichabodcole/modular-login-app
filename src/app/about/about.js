angular.module( 'lgApp.about', [
  'ui.router'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'about', {
    url: '/about',
    controller: 'AboutController',
    templateUrl: 'about/about.tpl.html',
    authenticate: true
  });
})

.controller( 'AboutController', function AboutCtrl( $scope ) {

});
