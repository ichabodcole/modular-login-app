angular.module('lgApp.login')

.controller( 'LoginController', function LoginController ($scope, $state, Authentication) {
  $scope.login = function(e){
    e.preventDefault();
    Authentication.login();
    $state.transitionTo('home');
  };
});