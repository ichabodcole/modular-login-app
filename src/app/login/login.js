angular.module('lgApp.login', [
  'ui.router',
  'lgApp.security.Authentication'
])

.config( function config( $stateProvider ) {
  $stateProvider.state( 'login', {
    url: '/login',
    templateUrl: 'login/login.tpl.html',
    controller: 'LoginController',
    authenticate: false,
    data:{ pageTitle: 'Login' }
  });
});