angular.module( 'lgApp', [
  'templates-app',
  'templates-common',
  'ui.router',
  'lgApp.nav',
  'lgApp.home',
  'lgApp.about',
  'lgApp.login',
  'lgApp.security'
])

.config(function ( $stateProvider, $urlRouterProvider ) {
  // Go to the index page if this page doesn't exist
  $urlRouterProvider.otherwise('/login');
})

.run( function ($rootScope, $state, Authentication) {
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
    if(toState.authenticate && !Authentication.isLoggedIn()){
      event.preventDefault();
      $state.transitionTo('login');
    }
  });
})

.controller( 'AppController', function ( $scope ) {

});

