angular.module('lgApp.nav', [])

.controller('NavController', function ($rootScope, $scope) {
  var model = {
    state: null
  };

  $scope.model = model;

  $scope.isActive = function (state) {
    return model.state === state;
  };

  $rootScope.$on('$stateChangeStart', function(event, toState, fromState) {
    model.state = toState.name;
  });
});