describe( 'navigation', function () {
  beforeEach( module('lgApp.nav') );

  describe('NavController', function() {
    var $scope, ctrl;

    beforeEach(inject(function($rootScope, $controller){
      $scope = $rootScope.$new();
      ctrl = $controller('NavController', {
        $scope: $scope
      });
    }));

    describe('isActive', function(){
      it('should return false when the argument does not match the model state', function () {
        $scope.model.state = 'about';
        expect($scope.isActive('home')).toBe(false);
      });
      it('should return true when the argument matches the model state', function () {
        $scope.model.state = 'home';
        expect($scope.isActive('home')).toBe(true);
      });
    });
  });
});