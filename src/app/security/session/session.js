angular.module('lgApp.security.Session', [])

  .factory('Session', function () {
    return {
      currentUser: null
    };
  });