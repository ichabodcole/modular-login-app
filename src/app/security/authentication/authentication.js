angular.module('lgApp.security.Authentication', ['lgApp.security.Session'])
  .factory('Authentication', function (Session) {
    return {
      login: function (user) {
        Session.currentUser = user;
      },
      isLoggedIn: function() {
        return Session.currentUser !== null;
      }
    };
  });