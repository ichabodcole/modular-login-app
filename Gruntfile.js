module.exports = function ( grunt ) {
  /**
   * Time how long tasks take. Can help when optimizing build times
   */
  require('time-grunt')(grunt);
  /**
   * Load in our build configuration file.
   */
  var userConfig = require( './build.config.js' );

  /**
   * This is the configuration object Grunt uses to give each plugin its
   * instructions.
   */
  var taskConfig = {
  /**
   * We read in our `package.json` file so we can access the package name and
   * version. It's already there, so we don't repeat ourselves here.
   */
    pkg: grunt.file.readJSON("package.json"),
    env: process.env
  };

  /**
   * Merge all the config files together so we can pass them to grunt.
   */
  var gruntConfig = grunt.util._.extend( taskConfig, userConfig );


  /**
   * Load required Grunt tasks. These are installed based on the versions listed
   * in `package.json` when you do `npm install` in this directory.
   */
  var path = require('path');
  /**
   * Use the load-grunt-config task to load in all task config files
   * located in ./tasks/options/.
   */
  require('load-grunt-config')(grunt, {
  configPath: path.join(process.cwd(), 'tasks/options/'),
  init:true,
  config: gruntConfig,
  loadGruntTasks: {}
  });

  /**
   * In order to make it safe to just compile or copy *only* what was changed,
   * we need to ensure we are starting from a clean, fresh build. So we rename
   * the `watch` task to `delta` (that's why the configuration var above is
   * `delta`) and then add a new task called `watch` that does a clean build
   * before watching for changes.
   */
  // grunt.renameTask( 'watch', 'delta' );
  // grunt.registerTask( 'watch', [ 'build', 'karma:unit', 'delta' ] );

  /**
   * The default task is to build and compile.
   */
  grunt.registerTask( 'default', [ 'build', 'compile' ] );

  grunt.registerTask( 'serve', function(target){
    if (target === 'dist') {
      return grunt.task.run(['compile', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'build',
      'karma:unit',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask( 'test', function(target){
    if(target === 'e2e') {
      return grunt.task.run([
        'copy:e2e',
        'connect:test',
        'protractor:test'
      ]);
    }

    if(target === 'unit') {
      return grunt.task.run([
        'karmaconfig',
        'karma:continuous',
      ]);
    }

    grunt.task.run([
      'build',
      'karmaconfig',
      'karma:continuous',
      'copy:e2e',
      'connect:test',
      'protractor:test'
    ]);
  });

  /**
   * The `build` task gets your app ready to run for development and testing.
   */
  grunt.registerTask( 'build', [
    'clean:dist',
    'html2js',
    'jshint',
    'sass:build',
    'concurrent:build_copy',
    'responsive_images',
    'injector:build'
  ]);

  /**
   * The `compile` task gets your app ready for deployment by concatenating and
   * minifying your code.
   */
  grunt.registerTask( 'compile', function(target){
    var tasks = [
      'sass:compile',
      'copy:compile',
      'imagemin',
      'ngmin',
      'concat:compile_js',
      'uglify',
      'injector:compile',
      'uncss',
      'cssmin'
    ];

    if (target === 'no-test') {
      tasks.unshift('build');
    } else {
      tasks.unshift('test');
    }

    grunt.task.run(tasks);
  });

  /**
   * Generate the docs for source comments and serve them up fresh!
   */
  grunt.registerTask( 'docs' , [
    'docular',
    'docular-server'
  ]);

  grunt.loadTasks('tasks');
};
