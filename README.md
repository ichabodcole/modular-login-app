
## Quick Start

This build systems assumes you have Ruby and Node installed, along with the Rubygems manager. In addition to regular npm dependencies you will need to install the sass gem and the graphicsmagick library which is used by grunt-responsive-images to generate additional sizes for each image.

After the above are correctly installed running the below should install everything thing you need, build the files and run the unit tests.

```sh
$ echo "INSTALL NPM"
$ sudo npm install
$ echo "INSTALL GRUNT-CLI"
$ sudo npm install grunt-cli -g
$ echo "INSTALL BOWER"
$ sudo npm install bower -g
$ echo "BOWER INSTALL"
$ bower install
$ ./node_modules/grunt-protractor-runner/node_modules/.bin/webdriver-manager update
$ echo "NPM TEST"
$ npm test
```
Happy hacking!

### Overall Directory Structure

At a high level, the structure looks roughly like this:

```
ng-boilerplate/
  |- tasks/
  |- karma/
  |- src/
  |  |- app/
  |  |  |- <app logic>
  |  |- assets/
  |  |  |- <static files>
  |  |- common/
  |  |  |- <reusable code>
  |  |- sass/
  |  |  |- main.sass
  |- vendor/
  |  |- angular-bootstrap/
  |  |- bootstrap/
  |- .bowerrc
  |- bower.json
  |- build.config.js
  |- deploy.sh
  |- drone.sh
  |- Gruntfile.js
  |- module.prefix
  |- module.suffix
  |- package.json
  |- protractor.config.js
```


### Grunt commands

- `grunt docs`

  Generate and serve docular docs:
  This command will launch a server and the docs will be available at localhost:8000
- `grunt serve`

  Build and serve the app from localhost:9000 for development. The source files will be watched for changes
  and livereload the browser after source files are saved. To serve the minimized production files run `grunt serve:dist`.
- `grunt build`

  Builds the app source for developement mode.
- `grunt compile`

  Builds the app source and prepares it for production (concatincation and minification) then copies the files into the dist directory. To compile without tests use `grunt compile:no-test`.
- `grunt test`

  Builds the source files then runs the karma unit tests and protractor e2e tests. To run only the `e2e` tests without building use `grunt test:e2e`. To run only the `unit` tests use `grunt test:unit`.

### Docular
  This project used docular to generate project / code documentation from source code and .doc files within the project. For more information see http://grunt-docular.com/.

### Git repos, Continuous Integration, and Deployment.
  This project will be hosted in a private repo on http://bitbucket.org.
  After pushing to bitbucket a post hook payload will be sent to Drone.io a hosted continuous integration application. Drone.io will build the project, run unit tests, minimize the source and finally deploy it to a password protected AWS ec2 instance.


## Philosophy of ngBoilerplate
### This section is taken from the original ngBoilerplate README.md. See http://joshdmiller.github.io/ng-boilerplate/#/home for more information.

The principal goal of `ngBoilerplate` is to set projects up for long-term
success.  So `ngBoilerplate` tries to follow best practices everywhere it can.
These are:

- Properly orchestrated modules to encourage drag-and-drop component re-use.
- Tests exist alongside the component they are testing with no separate `test`
  directory required; the build process should be sophisticated enough to handle
  this.
- Speaking of which, the build system should work automagically, without
  involvement from the developer. It should do what needs to be done, while
  staying out of the way. Components should end up tested, linted, compiled,
  and minified, ready for use in a production environment.
- Integration with popular tools like Bower, Karma, and LESS.
- *Encourages* test-driven development. It's the only way to code.
- A directory structure that is cogent, meaningful to new team members, and
  supporting of the above points.
- Well-documented, to show new developers *why* things are set up the way they
  are.
- It should be responsive to evidence. Community feedback is therefore crucial
  to the success of `ngBoilerplate`.

But `ngBoilerplate` is not an example of an AngularJS app: this is a
kickstarter. If you're looking for an example of what a complete, non-trivial
AngularJS app that does something real looks like, complete with a REST backend
and authentication and authorization, then take a look at
[`angular-app`](http://github.com/angular-app/angular-app), which does just
that - and does it well.


