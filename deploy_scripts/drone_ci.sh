#!/bin/bash
# This file is run by Drone.io a continuous integration app.
# The code is then deployed to an aws ec2 server, which is handled by drone.io.
# http:// drone.io
echo "RUNNING DRONE SETUP"

# This script updates and installs dependencies for the drone.io environment
# so that the app will build and test properly.
INSTALL_DEPS () {
  echo "INSTALL_DEPS"
  echo "UPDATE APT-GET"
  sudo apt-get update
  echo "INSTALL RUBYGEMS"
  sudo apt-get install rubygems
  echo "INSTALL SASS GEM"
  sudo gem install sass
  echo "INSTALL GRAPHICSMAGICK"
  sudo apt-get install graphicsmagick
  echo "INSTALL NPM"
  sudo npm -q install
  echo "INSTALL GRUNT-CLI"
  sudo npm -q install grunt-cli -g
  echo "INSTALL BOWER"
  sudo npm -q install bower -g
  echo "BOWER INSTALL"
  bower install
  echo "START XWINDOWS SESSION"
  sudo start xvfb
}

# Install the necessary dependencies for the drone.io instance
# if the drone variable is true. This var is set on the drone.io app to true.
if [[ $DRONE_SETUP == true ]]; then
  INSTALL_DEPS
fi

# Run when we want to compile a dist directory with files to put on aws.
GRUNT_COMPILE () {
  echo "RUN GRUNT COMPILE"
  grunt compile:no-test test:unit
}

# Run when we don't want to deploy files to aws
# since no dist folder will be generated.
GRUNT_BUILD () {
  echo "RUN GRUNT BUILD"
  grunt build test:unit
}

# First argument is the name of the folder to deploy too
# Second optional argument is a sub-folder
CREATE_DEPLOY_FOLDER () {
  echo "RUN ${1} DEPLOY SCRIPT"
  TMP_DIR=".tmp/${1}"

  # if the second argument is not empty create a subfolder
  # within the main folder
  if [[ ${2} != "" ]]; then
    SUBFOLDER="$TMP_DIR/${2}"
    echo "MAKE $SUBFOLDER DIRECTORY"
    mkdir -p "$SUBFOLDER"
    echo "MOVE .dist CONTENTS TO .tmp/${1} DIRECTORY"
    mv ./dist/* $SUBFOLDER
  else
    echo "MAKE .tmp/${1} DIRECTORY"
    mkdir -p $TMP_DIR
    echo "MOVE .dist CONTENTS TO .tmp/${1} DIRECTORY"
    mv ./dist/* $TMP_DIR
  fi

  echo "MOVE .tmp/${1} DIRECTORY TO .dist"
  mv $TMP_DIR ./dist
}

#
HASH=$(git rev-parse HEAD)
DATE=$(date +"%Y-%m-%d_%T")
DATE_HASH="${DATE}_${HASH}"
BRANCH_NAME="$(git rev-parse --abbrev-ref HEAD)"

if [[ $BRANCH_NAME == "master" ]]; then
  echo "RUN $BRANCH_NAME/$DATE_HASH DEPLOY SCRIPT"
  GRUNT_COMPILE
  CREATE_DEPLOY_FOLDER "$BRANCH_NAME" "$DATE_HASH"

elif [[ $BRANCH_NAME == "development" ]]; then
  echo "RUN $BRANCH_NAME/$DATE_HASH DEPLOY SCRIPT"
  GRUNT_COMPILE
  CREATE_DEPLOY_FOLDER "$BRANCH_NAME" "$DATE_HASH"

elif [[ $BRANCH_NAME == QA-* ]]; then
  echo "RUN QA/$BRANCH_NAME/$DATE_HASH DEPLOY SCRIPT"
  GRUNT_COMPILE
  CREATE_DEPLOY_FOLDER "QA" "$BRANCH_NAME/$DATE_HASH"

else
  echo "NO DEPLOYMENT SCRIPT FOR BRANCH $BRANCH_NAME"
  echo "-- DEPLOYMENT BRANCHES INCLUDE master, development and QA-*"
  GRUNT_BUILD
fi
