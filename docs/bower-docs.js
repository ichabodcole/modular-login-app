/**
* @doc overview
* @id index
* @name
* @description #Bower components and You.
*
* This will be an over view of the bower components used in this project.
*
* 1. Bower components are located in the ./vendor folder.
*/

/**
* @doc module
* @name bower
* @description A list of bower components we use.
*/

/**
* @doc interface
* @name bower.components:angular
*
* @description
* The angular library
*
* Location: ./app/bower_components/angular
*/

/**
* @doc interface
* @name bower.components:imager
*
* @description
* A library to include responsive images in your webpage.
*
* Location: ./app/bower_components/imager.js
*/

/**
* @doc interface
* @name bower.components:sass-bootstrap
*
* @description
* A sass implementation of the boostrap framework.
*
* Location: ./app/bower_components/sass-bootstrap
*/

/**
* @doc interface
* @name bower.components:jquery
*
* @description
* The one ring/library to bring them all and in the darkness bind them.
*
* Location: ./app/bower_components/jquery
*/

/**
* @doc interface
* @name bower.components:lodash
*
* @description
* A utility library delivering consistency, customization, performance, & extras.
*
* Location: ./app/bower_components/lodash
*/

/**
* @doc interface
* @name bower.components:restangular
*
* @description
* AngularJS service to handle Rest API Restful Resources properly and easily
*
* Location: ./app/bower_components/restangular
*/

/**
* @doc interface
* @name bower.components:angular-ui-router
*
* @description
* UI-Router for Nested Routing by the AngularUI Team! http://angular-ui.github.io/ui-router/site
*
* Location: ./app/bower_components/angular-ui-router
*/
