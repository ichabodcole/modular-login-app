/**
* @doc overview
* @id index
* @name
* @description #Grunt Tasks and You.
*
* This will be an over view of the grunt configuration.
*
* 1. Custom grunt tasks and grunt helpers are in the ./tasks folder.
* 2. All grunt task configuration files are in the ./tasks/options folder.
*/

/**
* @doc module
* @name tasks
* @description A list of tasks we use for for Grunt
*/