/**
* @doc interface
* @name tasks.tasks:karmaconfig
*
* @description
* In order to avoid having to specify manually the files needed for karma to
* run, we use grunt to manage the list for us. The `karma/*` files are
* compiled as grunt templates for use by Karma. Yay!
*
* Location: ./tasks/karmaconfig.js
*/

'use strict';
var path = require('path');
module.exports = function( grunt ) {

  var Helpers = require('../tasks/helpers');

  grunt.registerMultiTask( 'karmaconfig', 'Process karma config templates', function () {
    var tplFile = this.data.tpl,

    destFile = tplFile.replace('.tpl', ''),

    config_src = path.join(grunt.config('karma_dir'), tplFile),

    config_dest = path.join(grunt.config( 'build_dir' ),  destFile),

    scriptFiles = Helpers.filterForJS( this.filesSrc );

    grunt.file.copy( config_src, config_dest, {
      process: function ( contents, path ) {
        return grunt.template.process( contents, {
          data: {
            scripts: scriptFiles
          }
        });
      }
    });
  });
};