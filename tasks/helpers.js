var Helpers = {};
var grunt = require('grunt');
/**
 * A utility function to get all app JavaScript sources.
 */
Helpers.filterForJS = function ( files ) {
  return files.filter( function ( file ) {
    return file.match( /\.js$/ );
  });
};

/**
 * A utility function to get all app CSS sources.
 */
Helpers.filterForCSS = function ( files ) {
  return files.filter( function ( file ) {
    return file.match( /\.css$/ );
  });
};

module.exports = Helpers;