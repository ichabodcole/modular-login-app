/**
* @doc interface
* @name tasks.options:karma
*
* @description
* A grunt wrapper for the karma test runner.
* See the karma-unit.js file in the test_config directory for karma configuration settings
*
* Location: ./tasks/options/karma.js
*/
module.exports = {
  options: {
    configFile: '<%= build_dir %>/karma-unit.js'
  },
  unit: {
    background: true,
    port: 9101
  },
  continuous: {
    singleRun: true
  }
}