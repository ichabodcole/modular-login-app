/**
* @doc interface
* @name tasks.options:responsive_images
*
* @description
* Produce images at different sizes for responsive websites.
*
* Location: ./tasks/options/responsive_images.js
*/

module.exports = {
  options: {
    engine: 'gm'
    // sizes: [{
    //   name: 'small',
    //   width: 320,
    //   upscale: true
    // },{
    //   name: 'medium',
    //   width: 640,
    //   upscale: true
    // },{
    //   name: 'large',
    //   width: 1024,
    //   upscale: true
    // }]
  },
  dev: {
    files: [{
      expand: true,
      cwd: '<%= build_dir %>/assets/images/',
      src: ['**.{jpg,gif,png}', '!responsive/**/*.{jpg,gif,png}'],
      dest: '<%= build_dir %>/assets/images/responsive/'
    }]
  }
}