/**
* @doc interface
* @name tasks.options:karmaconfig
*
* @description
* This task compiles the karma template so that changes to its file array
* don't have to be managed manually.
*
* Location: ./tasks/options/karmaconfig.js
*/
module.exports = {
  karma: {
    tpl: 'karma-unit.tpl.js',
    files: {
      dir: '<%= build_dir %>',
      src: [
        '<%= vendor_files.js %>',
        '<%= html2js.app.dest %>',
        '<%= html2js.common.dest %>',
        '<%= test_files.js %>'
      ]
    }
  }
}