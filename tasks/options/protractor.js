/**
* @doc interface
* @name tasks.options:protactor
*
* @description
* A grunt wrapper for the protractor test runner.
* See the protractor.config.js file in the test_config directory for protractor configuration settings
*
* Location: ./tasks/options/protractor.js
*/
module.exports = {
  options: {
    configFile: './protractor.config.js'
  },
  test:{
    // options: {
    //   args: {
    //       baseUrl:'http://127.0.0.1:9001/test/'
    //   }
    // }
  }
}