/**
* @doc interface
* @name tasks.options:concurrent
*
* @description
* Run some tasks in parallel to speed up the build process
*
* Location: ./tasks/options/concurrent.js
*/
module.exports = {
  // server: [
  //   'sass:server',
  //   'html2js'
  // ],
  // test: [
  //   'sass',
  //   'html2js'
  // ],
  // dist: [
  //   'sass:dist',
  //   'html2js',
  //   'imagemin',
  //   'svgmin'
  // ]
  build: [
    'html2js',
    'jshint',
    'sass:build'
  ],
  build_copy: [
    'copy:build_app_assets',
    'copy:build_vendor_assets',
    'copy:build_app_js',
    'copy:build_vendor_js',
    'copy:build_vendor_css',
    'copy:build_html'
  ]
}