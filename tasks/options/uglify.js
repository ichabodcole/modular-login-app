/**
* @doc interface
* @name tasks.options:uglify
*
* @description
* Minifies your javascript files.
*
* Location: ./tasks/options/uglify.js
*/
module.exports = {
  compile: {
    options: {
      banner: '<%= meta.banner %>'
    },
    files: {
      '<%= concat.compile_js.dest %>': '<%= concat.compile_js.dest %>'
    }
  }
}