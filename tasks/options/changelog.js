/**
* @doc interface
* @name tasks.options:changelog
*
* @description
* Creates a changelog on a new version.
*
* Location: ./tasks/options/changelog.js
*/

module.exports = {
  options: {
    dest: 'CHANGELOG.md',
    template: 'changelog.tpl'
  }
}