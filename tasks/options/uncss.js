/**
* @doc interface
* @name tasks.options:uncss
*
* @description
* Removes unused css. This is especially useful for vendor libraries like bootstrap where much of the css goes unused.
*
* Location: ./tasks/options/uncss.js
*/
module.exports = {
  dist: {
    options: {
      // stylesheets: ['./bower_components/sass-bootstrap/dist/css/bootstrap.css'],
    },
    files: {
      // The below arguments point to the sass tasks compile.dest value, located at ./tasks/options/sass.js.
      '<%= sass.compile.dest %>': ['<%= compile_dir %>/index.html']
    }
  }
}