/**
* @doc interface
* @name tasks.options:copy
*
* @description
* The `copy` task just copies files from A to B. We use it here to copy
* our project assets (images, fonts, etc.) and javascripts into
* `build_dir`, and then to copy the assets to `compile_dir`.
*
* Location: ./tasks/options/copy.js
*/

module.exports = {

  build_app_assets: {
    files: [
      // Move add js files to the build/src directory.
      {
        expand: true,
        cwd: '<%= app_dir %>/assets/',
        src: [ '**' ],
        dest: '<%= build_dir %>/assets/'
      }
    ]
  },

  build_app_js: {
    files: [
      // Move add js files to the build/src directory.
      {
        expand: true,
        cwd: '.',
        src: [ '<%= app_files.js %>' ],
        dest: '<%= build_dir %>/'
      }
    ]
  },

  build_html: {
    files: [
      // Move any app level html files to the build directory.
      {
        expand: true,
        cwd: '<%= app_dir %>',
        src: [ '*.html' ],
        dest: '<%= build_dir %>'
      }
    ]
  },

  build_vendor_assets: {
    files: [
      // Move any vendor assets to the build directory.
      {
        expand: true,
        flatten: true,
        cwd: '.',
        src: [ '<%= vendor_files.assets %>' ],
        dest: '<%= build_dir %>/assets/'
      }
    ]
  },

  build_vendor_js: {
    files: [
      // Move all vendor js files to the build directory.
      {
        expand: true,
        cwd: '.',
        src: [ '<%= vendor_files.js %>' ],
        dest: '<%= build_dir %>/'
      }
    ]
  },

  build_vendor_css: {
    files: [
      // Move all vendor js files to the build directory.
      {
        expand: true,
        cwd: '.',
        src: [ '<%= vendor_files.css %>' ],
        dest: '<%= build_dir %>/'
      }
    ]
  },

  e2e: {
    files: [
      // Move all e2e js files to the build directory.
      {
        expand: true,
        cwd: '.',
        src: [ '<%= app_files.jse2e %>' ],
        dest: '<%= build_dir %>/'
      }
    ]
  },

  compile: {
    files: [
      // Move any app level html files from the build directory to the compile directory.
      {
        expand: true,
        cwd: '<%= build_dir %>',
        src: [ '*.html' ],
        dest: '<%= compile_dir %>'
      },
      // Move all the asset files from the build directory to the compile directory.
      {
        expand: true,
        cwd: '<%= build_dir %>/assets',
        src: [ '**' ],
        dest: '<%= compile_dir %>/assets'
      }
    ]
  }
}