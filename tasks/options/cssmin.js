/**
* @doc interface
* @name tasks.options:cssmin
*
* @description
* Minifies your css
* We need to use this because uncss generates unminified code, so during the compile task
* after uncss runs we do a final pass with cssmin.
*
* Location: ./tasks/options/cssmin.js
*/
module.exports = {
  uncss: {
    files: {
      // The below arguments point to the sass tasks compile.dest value, located at ./tasks/options/sass.js.
      '<%= sass.compile.dest %>': ['<%= sass.compile.dest %>']
    }
  }
}