/**
* @doc interface
* @name tasks.options:injectory
*
* @description
* Injects scripts and css files into our html
*
* Location: ./tasks/options/injector.js
*/
module.exports = {
  options: {
    addRootSlash: false
  },
  build: {
    options: {
      ignorePath: '.tmp/'
    },
    files: {
      '<%= build_dir %>/index.html': [
        '<%= vendor_files.js %>',
        '<%= vendor_files.css %>',
        '<%= build_dir %>/src/**/*.js',
        '<%= html2js.common.dest %>',
        '<%= html2js.app.dest %>',
        '<%= vendor_files.css %>',
        '<%= sass.build.dest %>'
      ]
    }
  },
  compile: {
    options: {
      ignorePath: ['.tmp/', 'dist/']
    },
    files: {
      '<%= compile_dir %>/index.html' : [
        '<%= concat.compile_js.dest %>',
        '<%= vendor_files.css %>',
        '<%= sass.compile.dest %>'
      ]
    }
  }
}