/**
* @doc interface
* @name tasks.options:clean
*
* @description
* Deletes the specified directories
*
* Location: ./tasks/options/clean.js
*/

module.exports = {
  dist: {
    files: [{
        dot: true,
        src: [
            '<%= build_dir %>',
            '<%= compile_dir %>*',
            '!<%= compile_dir %>/.git*'
        ]
    }]
  },
  server: '<%= build_dir %>'
}