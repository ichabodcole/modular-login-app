/**
* @doc interface
* @name tasks.options:docular
*
* @description
* Provides automated documentation the Angular way!
*
* Location: ./tasks/options/docular.js
*/
module.exports = {
  // docular_webapp_target: 'docs',
  groups: [
    {
      groupTitle: "Login App",
      groupId: "loginapp",
      groupIcon: "icon-glass",
      descr: "This is the group description",
      sections: [
        {
          id:"grunt",
          title: "Grunt Tasks",
          scripts: ['./docs/grunt-docs.js', './tasks'],
          showSource: true
        },
        {
          id:"bower",
          title: "Bower Components",
          scripts: ['./docs/bower-docs.js'],
          showSource: false
        }
      ]
    }
  ]
}