/**
* @doc interface
* @name tasks.options:ngmin
*
* @description
* Allow the use of non-minsafe AngularJS files.
* Automatically makes it minsafe compatible so Uglify does not destroy the ng references
*
* Location: ./tasks/options/ngmin
*/

module.exports = {
  compile: {
    files: [
      {
        src: [ '<%= app_files.js %>' ],
        cwd: '<%= build_dir %>',
        dest: '<%= build_dir %>',
        expand: true
      }
    ]
  }
}