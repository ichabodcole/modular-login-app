/**
* @doc interface
* @name tasks.options:imagemin
*
* @description
* Reduces the file size of your images
*
* Location: ./tasks/options/imagemin.js
*/
module.exports = {
  dist: {
    files: [{
      expand: true,
      cwd: '<%= build_dir %>/assets/images',
      src: '{,*/}*.{png,jpg,jpeg,gif}',
      dest: '<%= compile_dir %>/assets/images'
    }]
  }
}