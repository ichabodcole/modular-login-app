/**
* @doc object
* @name tasks.options:sass
*
* @description
*
* `sass` handles our SASS compilation and minification automatically.
* Only our `main.sass` file is included in compilation; all other files
* must be imported from this file.
*
* In our case we do not minify the output since cssmin will get run at the end of the
* compile task after uncss.
*
* Location: ./tasks/options/sass.js
*/

module.exports = {
  options: {
    loadPath: ['./vendor/', './vendor/bootstrap-sass/vendor/assets/stylesheets/bootstrap/']
  },
  build: {
    src: [ '<%= app_files.sass %>' ],
    dest: '<%= build_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
  },
  compile: {
    options: {
      style:'compressed'
    },
    // The below arguments point to this modules build.dest value.
    src: [ '<%= sass.build.dest %>' ],
    dest: '<%= sass.build.dest %>'
  }
}