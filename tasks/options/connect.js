/**
* @doc interface
* @name tasks.options:connect
*
* @description
* Create one or many local web servers at specified ports
*
* Location: ./tasks/options/connect.js
*/
module.exports = {
  options: {
    port: 9000,
    // Change this to '0.0.0.0' to access the server from outside.
    hostname: 'localhost',
    livereload: 35729
  },
  livereload: {
    options: {
      open: true,
      base: [
        '<%= build_dir %>'
      ]
    }
  },
  test: {
    options: {
      port: 9001,
      base: [
        'test',
        '<%= build_dir %>'
      ]
    }
  },
  dist: {
    options: {
      open: true,
      base: '<%= compile_dir %>',
      livereload: false
    }
  }
}