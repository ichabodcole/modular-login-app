/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {
  /**
   * The `build_dir` folder is where our projects are compiled during
   * development and the `compile_dir` folder is where our app resides once it's
   * completely built.
   */
  app_dir: 'src',
  build_dir: '.tmp',
  compile_dir: 'dist',
  vendor_dir: 'vendor',
  karma_dir: 'karma',

  /**
   * This is a collection of file patterns that refer to our app code (the
   * stuff in `src/`). These file paths are used in the configuration of
   * build tasks. `js` is all project javascript, sass tests. `ctpl` contains
   * our reusable components' (`src/common`) template HTML files, while
   * `atpl` contains the same, but for our app's code. `html` is just our
   * main HTML file, `sass` is our main stylesheet, and `unit` contains our
   * app's unit tests.
   */
  app_files: {
    js: [ 'src/**/*.js', '!src/**/*.spec.js', '!src/assets/**/*.js' ],
    jsunit: [ 'src/**/*.spec.js' ],
    jse2e: ['e2e/**/*.js'],

    atpl: [ 'src/app/**/*.tpl.html' ],
    ctpl: [ 'src/common/**/*.tpl.html' ],

    html: [ 'src/index.html' ],
    sass: 'src/sass/main.scss'
  },

  /**
   * This is a collection of files used during testing only.
   */
  test_files: {
    js: [
      'vendor/angular-mocks/angular-mocks.js'
    ]
  },

  /**
   * This is the same as `app_files`, except it contains patterns that
   * reference vendor code (`vendor/`) that we need to place into the build
   * process somewhere. While the `app_files` property ensures all
   * standardized files are collected for compilation, it is the user's job
   * to ensure non-standardized (i.e. vendor-related) files are handled
   * appropriately in `vendor_files.js`.
   *
   * The `vendor_files.js` property holds files to be automatically
   * concatenated and minified with our project source files.
   *
   * The `vendor_files.css` property holds any CSS files to be automatically
   * included in our app.
   *
   * The `vendor_files.assets` property holds any assets to be copied along
   * with our app's assets. This structure is flattened, so it is not
   * recommended that you use wildcards.
   */
  vendor_files: {
    js: [
      'vendor/jquery/dist/jquery.js',
      'vendor/angular/angular.js',
      'vendor/angular-bootstrap/ui-bootstrap-tpls.js',
      'vendor/angular-ui-router/release/angular-ui-router.js',
      'vendor/restangular/dist/restangular.js',
      'vendor/lodash/dist/lodash.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/affix.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/alert.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/button.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/carousel.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/collapse.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/dropdown.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/modal.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/popover.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/scrollspy.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/tab.js',
      // 'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/transition.js',
      'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/tooltip.js',
      'vendor/bootstrap-sass/vendor/assets/javascripts/bootstrap/*.js'
    ],
    css: [
    ],
    assets: [
    ]
  },
};
